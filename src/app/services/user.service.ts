import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) { }
  getUserById(id) {
    return this.http.get(HOST_URL + '/users/' + id);
  }
}
