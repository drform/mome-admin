import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(
    private http: HttpClient,
  ) { }
  getAnswer(id, order, limit, offset, keywords) {
    return this.http.get(HOST_URL + '/forms/' + id + '/user_forms?order=' + order + '&limit=' + limit + '&offset=' + offset + '&keyword=' + keywords);
  }
  getAnswerDetail(fid, aid) {
    return this.http.get(HOST_URL + '/forms/' + fid + '/user_forms/' + aid);
  }
  deleteAnswer(fid, usfid, message) {
    return this.http.delete(HOST_URL + '/forms/' + fid + '/user_forms/' + usfid + '?message=' + message);
  }
}
