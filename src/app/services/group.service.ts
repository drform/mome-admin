import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(
    private http: HttpClient,
  ) { }
  getGroup(order, limit, offset, keyword) {
    return this.http.get(HOST_URL + '/groups?order=' + order + '&limit=' + limit + '&offset=' + offset + '&keyword=' + keyword);
  }
}
