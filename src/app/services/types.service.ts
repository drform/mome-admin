import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class TypeService {

  constructor(
    private http: HttpClient,
  ) { }
  getType() {
    return this.http.get(HOST_URL + '/data_types/');
  }
}
