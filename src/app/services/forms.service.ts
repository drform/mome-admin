import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(
    private http: HttpClient,
  ) { }
  getForm(order, limit, offset, groupid, keywords) {
    return this.http.get(HOST_URL + '/forms?order=' + order + '&limit=' + limit + '&offset=' + offset + '&group_id=' + groupid + '&keyword=' + keywords);
  }
  getFormById(id) {
    return this.http.get(HOST_URL + '/forms/' + id);
  }
  updateForm(fid, params) {
    return this.http.patch(HOST_URL + 'forms/' + fid, params);
  }
  deleteForm(id, message) {
    return this.http.delete(HOST_URL + '/forms/' + id + '?message=' + message);
  }
  addForm(myForm) {
    return this.http.post(HOST_URL + '/forms', myForm);
  }
}
