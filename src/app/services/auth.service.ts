import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) { }

  postSignin(data) {
    const params = { session: data };
    return this.http.post(HOST_URL + '/login', params);
  }
  getUser(order, limit, offset, keywords, role) {
    return this.http.get(HOST_URL + '/users?order=' + order + '&limit=' + limit + '&offset=' + offset + '&keyword=' + keywords + '&role_id=' + role);
  }
  postUser(data) {
    return this.http.post(HOST_URL + '/users/', data);
  }
  getUserById(id) {
    return this.http.get(HOST_URL + '/users/' + id);
  }
  updateUser(body) {
    return this.http.put(HOST_URL + '/users', body);
  }
  deleteUser(id) {
    return this.http.delete(HOST_URL + '/users/' + id);
  }
  signout() {
    return this.http.delete(HOST_URL + '/logout/');
  }
  getAdmin() {
    return this.store.select('user');
  }
}
