export interface GroupList {
  id?: number;
  user_id?: number;
  description?: string;
  created_at?: Date;
  updated_at?: Date;
  image?: string;
}
