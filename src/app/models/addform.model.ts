export interface FormAdd {
  form?: {
    title?: string;
    description?: string;
    image?: string;
  };
  form_item?: any;
}

export interface FormItem {
  order_no: number;
  data_type: string;
  title: string;
  description: string;
  required: boolean;
  av_search: boolean;
  value: Dropdown[];
}

export interface Dropdown {
  data: string;
}

