export interface ShowAnswer {
  user_form?: Answer[];
  form?: Form;
}
export interface Answer {
  id?: number;
  user_id?: number;
  form_id?: number;
  created_at?: Date;
  updated_at?: Date;
  name?: string;
  image?: string;
}
export interface Form {
  id?: number;
  title?: string;
  description?: string;
  image?: string;
}

export interface AnswerDetail {
  data?: AnswerData[];
  creater?: AnswerCreater;
}
export interface AnswerData {
  id?: number;
  order_no?: number;
  data_type_id?: number;
  question?: string;
  answer?: AnswerValue[];
}
export interface AnswerValue {
  user_item_id?: number;
  value?: string;
}
export interface AnswerCreater {
  user_id?: number;
  username?: string;
  created_at?: Date;
  image?: string;
}

