import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import * as UserActions from '../../actions/user.actions';
import { Host } from '../../../environments/environment';
import { User } from '../../Models/user.model';
import { Location } from '@angular/common';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  HOST_URL = Host.url;
  admin: User;
  constructor(
    private routes: Router,
    private authService: AuthService,
    private store: Store<AppState>,
    private location: Location,
  ) { }

  ngOnInit() {
    this.authService.getAdmin().subscribe(data => {
      this.admin = data;
    });
  }

  signOut() {
    this.authService.signout().subscribe(data => {
      console.log(data);
    });
    this.store.dispatch(new UserActions.RemoveUser());
    this.routes.navigate(['/']);
  }
  destroyGroup() {
    localStorage.removeItem('groupid');
    this.location.path();
  }

}
