import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { GroupRoutingModule } from './group-routing.module';
import { GroupComponent } from '../group/group.component';

import { LayoutModule } from '../layout/layout.module';
@NgModule({
  declarations: [
    GroupComponent
  ],
  imports: [
    CommonModule,
    GroupRoutingModule,
    FormsModule,
    LayoutModule
  ]
})
export class GroupModule { }
