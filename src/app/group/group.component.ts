import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { GroupList } from '../Models/group.model';
import { GroupService } from '../services/group.service';
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

  constructor(
    private groupService: GroupService,
    private location: Location,
  ) { }
  keywords = '';
  order = 'id ASC';
  limit = 10;
  offset = 0;
  myGroups: GroupList;
  ngOnInit() {
    this.getGroups();
  }
  getGroups() {
    this.groupService.getGroup(this.order, this.limit, this.offset, this.keywords).subscribe(data => {
      this.myGroups = data;
    });
  }
  orderChange() {

  }
  backGroup() {

  }
  nextGroup() {

  }
  setGroupId(groupid) {
    localStorage.setItem('groupid', groupid);
  }
}
