import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './login/signin/signin.component';
import { AuthGuard } from './Services/auth.guard';
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/signin', pathMatch: 'full', },
      { path: 'signin', component: SigninComponent },
      {
        path: 'groups',
        loadChildren: './group/group.module#GroupModule',
      },
      {
        path: 'forms',
        loadChildren: './forms/forms.module#FormsModules',
      },
      {
        path: 'users',
        loadChildren: './users/users.module#UsersModule',
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
