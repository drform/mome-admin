import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { UserInfo } from '../../Models/user.model';
import { Router } from '@angular/router';
import { Host } from '../../../environments/environment';
import swal from 'sweetalert';
@Component({
  selector: 'app-showuser',
  templateUrl: './showuser.component.html',
  styleUrls: ['./showuser.component.css']
})
export class ShowuserComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private store: Store<AppState>,
    private routes: Router
  ) { }
  url = Host.url;
  id;
  myUser: UserInfo;
  formImage = [
    {
      url: '/assets/img/syringe.png',
      type: 'image/png'
    }
  ];
  selectedFiles: File;
  imageNameShow: any = 'Choose image';
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id')
    this.getUserById(this.id);
  }
  getUserById(id) {
    this.authService.getUserById(id).subscribe(data => {
      console.log(data);
      this.myUser = data;
    });
  }
  handleFileInput(event) {
    this.formImage = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.formImage.push({
          url: event.target.result,
          type: this.selectedFiles.type
        });
        this.imageNameShow = this.selectedFiles.name;
      }
      reader.readAsDataURL(this.selectedFiles);
    }
  }
  updateUser() {
    var imageText: any;
    let itemArray: any;

    for (let item of this.formImage) {
      if (item.type === 'image/jpeg') {
        imageText = item.url.split(',')[1];
      } else if (item.type === 'image/png') {
        imageText = item.url.split(',')[1];
      } else if (item.type === 'video/mp4') {
        imageText = item.url.split(',')[1];
      } else {
        imageText = '';
      }
      itemArray = imageText;
    }
    const body = {
      user: {
        user_id: this.myUser.id,
        name: this.myUser.name,
        email: this.myUser.email,
        tel: this.myUser.tel,
        position: this.myUser.position,
        image: itemArray
      }
    };
    swal({
      title: 'Are you sure update user?',
      icon: 'warning',
      buttons: ['Cancel', 'OK'],
    }).then((willDelete) => {
      if (willDelete) {
        this.authService.updateUser(body).subscribe(data => {
          swal({
            title: 'Add Success',
            text: data['message'],
            icon: 'success',
          });
          this.routes.navigate(['/users']);
        });
      } else {
        swal('Cancel');
      }
    });
  }
}
