import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../Models/user.model';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert';
@Component({
  selector: 'app-listuser',
  templateUrl: './listuser.component.html',
  styleUrls: ['./listuser.component.css']
})
export class ListuserComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private routes: Router,
    private location: Location
  ) { }
  order = 'id ASC';
  limit = 10;
  offset = 0;
  myUser: User;
  keywords = '';
  username;
  password;
  conpass;
  tel;
  email;
  roleFilter = 2;
  roleName = 'User';
  ITEM = 'Select Role';
  roleCreate;
  ngOnInit() {
    this.getUser();
  }
  getUser() {
    this.authService.getUser(this.order, this.limit, this.offset, this.keywords, this.roleFilter).subscribe(data => {
      const len = Object.keys(data).length;
      if (len !== 0) {
        this.myUser = data;
        console.log(this.myUser);
      }

    });
  }
  nextList() {
    this.offset += 10;
    if (this.offset <= 0) {
      this.offset = 0;
    }
    this.getUser();
  }
  backList() {
    this.offset -= 10;
    if (this.offset <= 0) {
      this.offset = 0;
    }
    this.getUser();
  }
  newUser() {
    swal({
      title: 'Are you sure to addning user?',
      icon: 'warning',
      buttons: ['Cancel', 'OK'],
    }).then((willDelete) => {
      if (willDelete) {
        if (this.password === this.conpass) {
          if (this.username && this.email && this.tel) {
            const datas = {
              user: {
                username: this.username,
                password: this.password,
                name: 'Insert your Name',
                email: this.email,
                tel: this.tel,
                position: 'Insert your Position',
                role_id: this.roleCreate
              }
            };
            this.authService.postUser(datas).subscribe(data => {
              swal({
                title: 'Good job!',
                text: data['message'],
                icon: 'success',
              });
              this.getUser();
            });
          } else {
            swal('Please fill data');
          }
        } else {
          swal('Password mismatch.');
        }
      } else {
        swal('Cancel.');
      }
    });
  }
  deleteUser(id) {
    swal({
      title: 'Are you sure delete user?',
      icon: 'warning',
      buttons: ['Cancel', 'OK'],
    }).then((willDelete) => {
      if (willDelete) {
        this.authService.deleteUser(id).subscribe(data => {
          swal({
            title: 'Delete Success',
            text: data['message'],
            icon: 'success',
          });
          this.getUser();
          this.routes.navigate(['/users']);
        });
      } else {
        swal('Cancel');
      }
    });
  }
  changeRole() {
    if (this.roleFilter === 2) {
      this.roleFilter = 3;
      this.roleName = 'User Admin';
    } else {
      this.roleFilter = 2;
      this.roleName = 'User';
    }
    this.getUser();
  }
  setRole(roleName, roleId) {
    this.ITEM = roleName;
    this.roleCreate = roleId;
  }
}
