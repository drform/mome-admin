import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UsersRoutingModule } from './users-routing.module';
import { LayoutModule } from '../layout/layout.module';
import { UsersComponent } from '../users/users.component';
import { ShowuserComponent } from '../users/showuser/showuser.component';
import { ListuserComponent } from './listuser/listuser.component';
@NgModule({
  declarations: [
    UsersComponent,
    ShowuserComponent,
    ListuserComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    LayoutModule,
    FormsModule
  ]
})
export class UsersModule { }
