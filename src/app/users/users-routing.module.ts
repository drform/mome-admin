import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from '../users/users.component';
import { ShowuserComponent } from '../users/showuser/showuser.component';
import { ListuserComponent } from './listuser/listuser.component';
const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      { path: 'show/:id', component: ShowuserComponent },
      { path: '', component: ListuserComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
