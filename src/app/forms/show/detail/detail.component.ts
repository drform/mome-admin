import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnswerService } from '../../../services/answer.service';
import { Host } from '../../../../environments/environment';
import { AnswerDetail } from '../../../Models/answerdetail.model';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private answerService: AnswerService
  ) { }
  url = Host.url;
  id;
  fid = localStorage.getItem('fid');
  myAnswerDetail: AnswerDetail;
  ngOnInit() {
    console.clear();
    this.getAnswerDetail();
  }
  getAnswerDetail() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.answerService.getAnswerDetail(this.fid, this.id).subscribe(data => {
      this.myAnswerDetail = data;
      console.log(this.myAnswerDetail.data);
    });
  }

}
