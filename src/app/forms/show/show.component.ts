import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Host } from '../../../environments/environment';

import { Location } from '@angular/common';
import { AnswerService } from '../../services/answer.service';
import { AnswerDetail } from '../../Models/answerdetail.model';
import { ShowAnswer } from '../../Models/answerdetail.model';

import { UserInfo } from '../../models/user.model';
import { UserService } from '../../services/user.service';

import { FormService } from '../../services/forms.service';
import { FormList } from '../../Models/formz.model';
import { forEach } from '@angular/router/src/utils/collection';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private answerService: AnswerService,
    private userService: UserService,
    private formService: FormService,
    private location: Location
  ) { }
  url = Host.url;
  id;
  order = 'id ASC';
  limit = 10;
  offset = 0;
  myAnswerDetail: AnswerDetail;
  myAnswer: ShowAnswer;
  senderUser: UserInfo;
  formClicked: FormList;
  keywords = '';
  message;
  answerIdClicked;
  aid;
  fid;
  dropdown = ['Drop 1', 'Drop 2', 'Drop N'];
  b = false;
  ob1;
  ngOnInit() {
    this.fid = this.route.snapshot.paramMap.get('id')
    this.getAnswer();
  }
  getAnswer() {
    console.clear();
    this.answerService.getAnswer(this.route.snapshot.paramMap.get('id'), this.order, this.limit, this.offset, this.keywords).subscribe(data => {
      this.myAnswer = data;
      console.log(this.myAnswer);
      this.ob1 = Object.keys(this.myAnswer.user_form).length;
      this.formService.getFormById(this.myAnswer.form.id).subscribe(dataz => {
        this.formClicked = dataz;
        this.userService.getUserById(this.formClicked.form.user_id).subscribe(user => {
          this.senderUser = user;
        });
      });
    });
  }
  getEmailAnswer(userid) {
    this.userService.getUserById(userid).subscribe(user => {
      this.senderUser = user;
      console.log(this.senderUser);
    });
  }
  getAnsweClicked(index) {
    this.answerIdClicked = {
      id: this.myAnswer.user_form[index].id,
      form_id: this.myAnswer.user_form[index].form_id
    };
    this.formService.getFormById(this.answerIdClicked.form_id).subscribe(data => {
      this.formClicked = data;
      this.userService.getUserById(this.formClicked.form.user_id).subscribe(user => {
        this.senderUser = user;
      });
    });
    this.getAnswerDetail();
  }
  getAnswerDetail() {
    console.clear();
    this.answerService.getAnswerDetail(this.answerIdClicked.form_id, this.answerIdClicked.id).subscribe(data => {
      this.myAnswerDetail = data;
      this.myAnswerDetail.data.forEach(element => {
        console.log(element);
        // console.log(element.data_type_id);
        // console.log(element.question);
      });
    });
  }
  public saveEmail(message: string): void {
    // ... save user email
    this.message = message;
    if (message !== '') {
      console.log(this.message);
      this.deleteAnswer();
    }
  }

  public handleRefusalToSetEmail(dismissMethod: string): void {
    // dismissMethod can be 'cancel', 'overlay', 'close', and 'timer'
    console.log(dismissMethod);
  }
  deleteAnswer() {
    console.clear();
    console.log(this.answerIdClicked);
    this.answerService.deleteAnswer(this.answerIdClicked.form_id, this.answerIdClicked.id, this.message).subscribe(data => {
      console.log(data);
      this.getAnswer();
      this.location.path();
    });
  }
  nextAnswer() {
    this.myAnswerDetail = null;
    this.b = false;
    this.offset += 10;
    this.getAnswer();
  }
  backAnswer() {
    this.myAnswerDetail = null;
    this.b = false;
    this.offset -= 10;
    if (this.offset <= 0) {
      this.offset = 0;
    }
    this.getAnswer();
  }
  setFormId() {
    localStorage.setItem('fid', this.id);
  }

}
