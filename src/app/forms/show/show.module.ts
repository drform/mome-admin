import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowRoutingModule } from './show-routing.module';
import { ShowComponent } from '../show/show.component';
import { DetailComponent } from '../show/detail/detail.component';
import { FormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
@NgModule({
  declarations: [
    ShowComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    ShowRoutingModule,
    FormsModule,
    SweetAlert2Module
  ]
})
export class ShowModule { }
