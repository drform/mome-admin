import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { Host } from '../../../environments/environment';
import { Router } from '@angular/router';

import { ActivatedRoute } from '@angular/router';

import { FormList } from '../../models/formz.model';
import { FormService } from '../../services/forms.service';

import { UserInfo } from '../../models/user.model';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  HOST_URL = Host.url;
  myForm: FormList;
  senderUser: UserInfo;
  message = '';
  idDropdow;
  myUser: UserInfo;
  formImage = [
    {
      url: '/assets/img/syringe.png',
      type: 'image/png'
    }
  ];
  selectedFiles: File;
  imageNameShow: any = 'Choose image';
  constructor(
    private route: ActivatedRoute,
    private routes: Router,
    private formService: FormService,
    private userService: UserService,
    private location: Location
  ) { }
  BackLastPage() {
    this.location.path();
  }
  setIdDropdown(id) {
    this.idDropdow = id;
    console.log(this.idDropdow);
  }
  getForm() {
    this.formService.getFormById(this.route.snapshot.paramMap.get('id')).subscribe(data => {
      this.myForm = data;
      console.log(this.myForm);
    });
  }
  getEmailForm() {
    this.userService.getUserById(this.myForm.form.user_id).subscribe(data => {
      this.senderUser = data;
      console.log(this.senderUser);
    });
  }
  updateForm() {
    let body = {
      message: this.message,
      form: {
        user_id: this.myForm.form.user_id,
        group_id: this.myForm.form.group_id,
        title: this.myForm.form.title,
        description: this.myForm.form.description,
        image: []
      },
      form_item: []
    };
    this.myForm.form.question.forEach(element => {
      const temp = {
        id: element.id,
        order_no: element.order_no,
        data_type: element.data_type,
        title: element.title,
        description: element.description,
        required: element.required,
        av_search: element.av_search,
        value: []
      };
      if (element.data_type_id === 9) {
        element.dropdown.forEach(elementz => {
          const t = {
            id: elementz.id,
            data: elementz.dropdown_value
          };
          temp.value.push(t);
        });
      }
      body.form_item.push(temp);
    });
    if (this.formImage) {
      var imageText: any;
      let itemArray: any;

      for (let item of this.formImage) {
        if (item.type === 'image/jpeg') {
          imageText = item.url.split(',')[1];
        } else if (item.type === 'image/png') {
          imageText = item.url.split(',')[1];
        } else if (item.type === 'video/mp4') {
          imageText = item.url.split(',')[1];
        } else {
          imageText = '';
        }
        itemArray = imageText;
      }
      body.form.image = itemArray;
    } else {
      body.form.image = [];
    }
    if (this.message !== '') {
      this.formService.updateForm(this.route.snapshot.paramMap.get('id'), body).subscribe(data => {
        console.log(data);
        this.routes.navigate(['/forms']);
      });
    }
  }
  ngOnInit() {
    console.clear();
    this.getForm();
  }
  handleFileInput(event) {
    this.formImage = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFiles = <File>event.target.files[i];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.formImage.push({
          url: event.target.result,
          type: this.selectedFiles.type
        });
        this.imageNameShow = this.selectedFiles.name;
      }
      reader.readAsDataURL(this.selectedFiles);
    }
  }
}
