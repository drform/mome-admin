import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormService } from '../../services/forms.service';
import { Form } from '../../Models/formz.model';
import { FormList } from '../../Models/formz.model';

import { GroupList } from '../../Models/group.model';
import { GroupService } from '../../services/group.service';

import { TypeList } from '../../Models/types.model';
import { TypeService } from '../../services/types.service';

import { UserInfo } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { Location } from '@angular/common';

import { AnswerService } from '../../services/answer.service';
import { FormCount } from '../../Models/formz.model';
import { ShowAnswer } from '../../Models/answerdetail.model';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(
    private formService: FormService,
    private groupService: GroupService,
    private userService: UserService,
    private typeService: TypeService,
    private answerService: AnswerService,
    private routes: Router,
    private location: Location,

  ) { }
  myAnswer: ShowAnswer;
  order = 'id ASC';
  limit = 10;
  offset = 0;
  myForm: Form;
  formCount: FormCount;
  formClicked: FormList;
  senderUser: UserInfo;
  keywords = '';
  keygroup = '';
  groupid = localStorage.getItem('groupid');
  group: GroupList;
  datatype: TypeList;
  message = '';
  id;
  newTemp = [];
  total = [];
  lenz: number;
  onScrollDown() {
    console.log('scrolled down!!');
  }

  onScrollUp() {
    console.log('scrolled up!!');
  }
  orderChange() {
    console.clear();
    if (this.order === 'id ASC') {
      this.order = 'id DESC';
    } else {
      this.order = 'id ASC';
    }
    this.getForm();
  }
  addNewItem() {
    this.newTemp.push({
      title: '',
      description: '',
    });
  }
  ngOnInit() {
    this.getForm();
    this.getGroup();
  }
  getGroup() {
    this.groupService.getGroup(this.order, this.limit, this.offset, this.keygroup).subscribe(data => {
      this.group = data;
    });
    this.typeService.getType().subscribe(data => {
      this.datatype = data;
    });
  }
  getForm() {
    if (!this.groupid) {
      this.groupid = '';
    }
    this.formService.getForm(this.order, this.limit, this.offset, this.groupid, this.keywords).subscribe(data => {
      this.lenz = Object.keys(data).length;
      if (this.lenz !== 0) {
        this.myForm = data;
        this.formCount = data;
      }
      for (let index = 0; index < this.lenz; index++) {
        this.answerService.getAnswer(this.formCount[index].id, this.order, this.limit, this.offset, this.keywords).subscribe(answer => {
          this.myAnswer = answer;
          this.total[index] = Object.keys(this.myAnswer.user_form).length;
        });
      }
    });
  }
  nextForm() {
    this.offset += 10;
    this.getForm();
  }
  backForm() {
    this.offset -= 10;
    if (this.offset <= 0) {
      this.offset = 0;
    }
    this.getForm();
  }
  getEmailForm(id) {
    this.id = id;
    this.formService.getFormById(id).subscribe(data => {
      this.formClicked = data;
      this.userService.getUserById(this.formClicked.form.user_id).subscribe(user => {
        this.senderUser = user;
      });
    });
  }
  deleteForm() {
    if (this.message !== '') {
      this.formService.deleteForm(this.id, this.message).subscribe(data => {
        console.log(data);
        this.getForm();
      });
      this.location.path();
    }
  }
  setFormId(fid) {
    localStorage.setItem('fid', fid);
  }
}
