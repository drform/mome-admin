import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsComponent } from '../forms/forms.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {
    path: '',
    component: FormsComponent,
    children: [
      { path: '', component: ListComponent },
      {
        path: 'show/:id',
        loadChildren: 'src/app/forms/show/show.module#ShowModule',
      },
      {
        path: 'edit/:id', component: EditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
