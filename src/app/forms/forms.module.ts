import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '../layout/layout.module';
import { FormsRoutingModule } from './forms-routing.module';

import { FormsComponent } from '../forms/forms.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';

import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { MatDialogModule } from '@angular/material/dialog';
@NgModule({
  declarations: [
    FormsComponent,
    ListComponent,
    EditComponent,
  ],
  imports: [
    CommonModule,
    FormsRoutingModule,
    LayoutModule,
    FormsModule,
    SweetAlert2Module,
    MatDialogModule
  ],
  exports: [
    MatDialogModule
  ]
})
export class FormsModules { }
